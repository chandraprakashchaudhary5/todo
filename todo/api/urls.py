from django.urls import path
from rest_framework import routers
from todo.api.views import UserViewset, TodoViewset

router = routers.SimpleRouter()
router.register(r'signup', viewset=UserViewset, basename='signup')
router.register(r'todo', viewset=TodoViewset, basename='todo')

urlpatterns =[
    # path('token/', ExampleView.as_view(), name='token_obtain')
]
urlpatterns += router.urls