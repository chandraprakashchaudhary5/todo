from django.contrib.auth import get_user_model
from rest_framework import serializers

from todo.models import Todo

User = get_user_model()


class UserSignupSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        # fields = '__all__'
        fields = ('username', 'email', 'password')
        # excludes = '__all__'

    def create(self, validated_data):
        email = validated_data['email']
        username = validated_data['username']
        password = validated_data['password']
        user = User.objects.create(email=email, username=username)
        user.set_password(password)
        user.save()
        return validated_data



class Todoserializer(serializers.ModelSerializer):
    class Meta:
        model = Todo
        fields = ('title', 'description', 'created_by')

