from django.contrib.auth import get_user_model
from future.types.newbytes import unicode
from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticated

from todo.api.serializers import UserSignupSerializer, Todoserializer
from todo.models import Todo

User = get_user_model()


class UserViewset(mixins.CreateModelMixin,
                  viewsets.GenericViewSet):
    queryset = User.objects.all()
    serializer_class = UserSignupSerializer
    serializer_action_classes = {
        'create': UserSignupSerializer
    }

    def perform_create(self, serializer: UserSignupSerializer):
        if serializer.is_valid(raise_exception=True):
            serializer.save()

#
# from rest_framework.authentication import SessionAuthentication, BasicAuthentication
# from rest_framework.permissions import IsAuthenticated
# from rest_framework.response import Response
# from rest_framework.views import APIView
#
#
# class ExampleView(APIView):
#     authentication_classes = [SessionAuthentication, BasicAuthentication]
#     permission_classes = [IsAuthenticated]
#
#     def get(self, request, format=None):
#         content = {
#             'user': unicode(request.user),  # `django.contrib.auth.User` instance.
#             'auth': unicode(request.auth),  # None
#         }
#         return Response(content)


class TodoViewset(mixins.CreateModelMixin, mixins.ListModelMixin, mixins.UpdateModelMixin, mixins.RetrieveModelMixin, mixins.DestroyModelMixin,
                  viewsets.GenericViewSet):
    queryset = Todo.objects.all()
    permission_classes = [IsAuthenticated, ]
    serializer_class = Todoserializer
    serializer_action_classess={
        'create': Todoserializer
    }
